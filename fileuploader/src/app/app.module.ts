import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//import { BLOB_STORAGE_TOKEN, IBlobStorage } from './azure-storage/azureStorage';
//import { BlobStorageService } from './azure-storage/blob-storage.service';

import { DndComponent } from './dnd/dnd.component';
import { DndDirective } from './dnd/dnd.directive';

import {HttpClientModule} from '@angular/common/http';

/*
export function azureBlobStorageFactory(): IBlobStorage {
  return window['azureStorage'].Blob;
}
*/

@NgModule({
  declarations: [
    AppComponent,
    DndComponent,
    DndDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ 
    /*
    BlobStorageService,
    {
      provide: BLOB_STORAGE_TOKEN,
      useFactory: azureBlobStorageFactory
    }
    */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

