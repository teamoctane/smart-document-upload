import {Directive, HostListener, HostBinding} from '@angular/core';
//import {BlobService, UploadConfig, UploadParams} from 'angular-azure-blob-service';
//import { from  } from 'rxjs';
//import { combineAll, map } from 'rxjs/operators';
import { BlobStorageService } from '../azure-storage/blob-storage.service';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
 
import {HttpModule } from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';



//import { azureBlobStorageFactory } from '../app.module';

 import { ISasToken, IAzureStorage , BLOB_STORAGE_TOKEN, IBlobStorage } from '../azure-storage/azureStorage';
import { Http2SecureServer } from 'http2';

interface IUploadProgress {
  filename: string;
  progress: number;
}

export function azureBlobStorageFactory(): IBlobStorage {
  return window['AzureStorage'].Blob;
}

@Directive({
  selector: '[appDnd]',
  providers:[
    BlobStorageService,
    {
      provide: BLOB_STORAGE_TOKEN,
      useFactory: azureBlobStorageFactory
    }
  ]
})
 
export class DndDirective {
   filesSelected = false;
  constructor(
        private blobStorage: BlobStorageService,
        private httpClient: HttpClient
    ) {}

  @HostBinding('style.background') private background = '#eee';
  private percent = 0;
 
  @HostListener('dragover', ['$event']) public onDragOver(evt){
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#999';
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt){
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#eee'
  }

  @HostListener('drop', ['$event']) public onDrop(evt){
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#eee';
    let files = evt.dataTransfer.files;
    if(files != null && files.length > 0 && 
        (files[0].type.toLowerCase().indexOf('image') >= 0 || files[0].type.toLowerCase().indexOf('pdf') >= 0  )

      ){
       console.log('Valid file');
       let file = files[0];
       //1st if pdf call google service to change it into jpeg
       //Send the files[0] to the rest service call.
       //Then call the blob storage for storing.

 
      if (file !== null) {
        //This is the 1st step
        //TODO Uncomment below to upload file, commented out because testing out the OCR
      //  this.uploadFile(file);

        //This is the OCR component
        this.ocrProcessing(file);
      }
             
    }
    else{
      alert('Please upload a PDF or image file.');
    }
  }

  //This is the upload of files to the Microsoft Azure Blob Storage.  For successful 
  //calls it returns Etag and MD5 data
  uploadFile(file: File) {
    const account = {
      name: 'teamoctanestorage',
      sas: 'se=2019-04-26&sp=rwdlac&sv=2018-03-28&ss=b&srt=sco&sig=qOZsERiyNs3/bXvZntEeylmXnSTPbYWtG3xb5oyfsD0%3D'
    };

    const blobUri = 'https://' + account.name + '.blob.core.windows.net';
    const blobService = azureBlobStorageFactory().createBlobServiceWithSas(blobUri, account.sas);

    blobService.createBlockBlobFromBrowserFile('teamoctanestoragecontainer', 
        file.name, 
        file, 
         {},
        (error, result) => {
            if(error) {
                // Handle blob error
            } else {
                  const displayLabel = 'File ' +  file.name + ' was successfully uploaded.  Etag:' + result.etag + ", md5:" + result.contentSettings.contentMD5;
                console.log(displayLabel);
                alert(displayLabel);
                //TODO also pass the etag and MD5 to the COSMOS along with agent ID and data from OCR api
            }
        });
  }

  private mapProgress(file: File, progress: number): IUploadProgress {
    return {
      filename: file.name,
      progress: progress
    };
  }



  ocrProcessing(file: File ) {

      const ParseHeaders ={
        headers:  new HttpHeaders({
          'Content-Type' : 'application/octet-stream',
          //'Content-Type' : 'multipart/form-data',
          //'Content-Type' : 'application/json',
         'Ocp-Apim-Subscription-Key' : '6666446744bd4ae1bbe13c78b034d7da' 
      })
    };
    
    const OCR_URL = 'https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/recognizeText?handwriting=false';
     
    let formData:FormData = new FormData();
    formData.append('uploadFile', file, file.name);

 
  formData.append('Content-Type' , 'application/octet-stream');
  formData.append('Ocp-Apim-Subscription-Key' , '6666446744bd4ae1bbe13c78b034d7da');


    this.httpClient.post(OCR_URL, FormData, ParseHeaders)
    .subscribe(
        data => console.log(data),
        error =>  console.log(error),
        ()=> console.log("Done")
    );
  }
  

  private handleError (error: Response | any){
     console.error('ApiService::handleError', error);
     return error;
  }

  /*  This is for creating a container which has already been done
document.getElementById('create-button').addEventListener('click', () => {
    blobService.createContainerIfNotExists('teamoctanestoragecontainer',  (error, container) => {
        if (error) {
            // Handle create container error
        } else {
            console.log(container.name);
        }
    });
});

  */


  //Show the blobs that has been uploaded
  /*
  document.getElementById('list-button').addEventListener('click', () => {
    blobService.listBlobsSegmented('teamoctanestoragecontainer', null, (error, results) => {
        if (error) {
            // Handle list blobs error
        } else {
            results.entries.forEach(blob => {
                console.log(blob.name);
            });
        }
    });
    */



 


}
